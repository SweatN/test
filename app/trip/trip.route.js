function tripRoutes($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('trip', {
      abstract: true,
      component: 'tripList'
    })
    .state('new-trip', {
      url: '/trip/new',
      component: 'tripItem',
      params: {
        title: 'Create New Trip'
      }
    })
    .state('edit-trip', {
      url: '/trip/:id',
      component: 'tripItem',
      params: {
        title: 'Edit Trip'
      }
    })
    .state('new-plan', {
      url: '/trip/:tripId/plan/new',
      component: 'planItem',
      params: {
        title: 'New Plan'
      }
    })
    .state('edit-plan', {
      url: '/trip/:tripId/plan/:planId',
      component: 'planItem',
      params: {
        title: 'Edit Plan'
      }
    })
}

export default tripRoutes;
