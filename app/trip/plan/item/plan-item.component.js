import planItemHtml from './plan-item.component.html';

let planItemComponent = {
  template: planItemHtml,
  controller: ($scope, $log, $stateParams, Trips) => {

    $scope.title = $stateParams.title;
    $scope.isEditing = false;
    $scope.planTypes = Trips.getPlanTypes();
    $scope.plan = {
      type: 'flight',
      startDate: new Date(),
      endDate: new Date(),
      startTime: new Date(),
      endTime: new Date(),
      description: ''
    };

    Trips.get($stateParams.tripId).then((trip) => {

      $scope.trip = trip;
      $scope.datepickerOptions = {
        minDate: trip.startDate > Date.now() ? trip.startDate : new Date(),
        maxDate: $scope.trip.endDate
      };
      $scope.endDataDatepickerOptions = {
        minDate: new Date($scope.trip.startDate)
      };
      if (angular.isDefined($stateParams.planId)) {

        $scope.isEditing = true;
        $scope.plan = $scope.trip.plans[$stateParams.planId - 1];
        $scope.endDataDatepickerOptions = {
          minDate: new Date($scope.plan.startDate)
        };

      }

    });


    $scope.create = () => {

      if ($scope.plan.startDate > $scope.plan.endDate) {
        alert('Date is incorrect');
        return;
      }

      Trips.createPlan($stateParams.tripId, $scope.plan);
      alert('Plan is created');

    };

    $scope.update = () => {

      if ($scope.plan.startDate > $scope.plan.endDate) {
        alert('Date is incorrect');
        return;
      }

      Trips.update();
      alert('Plan is updated');

    };

    $scope.$watch(() => $scope.plan.startDate , (newValue, oldValue) => {

      $scope.endDataDatepickerOptions.minDate = new Date(newValue);

    });

  }

}

export default planItemComponent;
