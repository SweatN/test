import angular from 'angular';

function PlanService() {

  function Plan(data) {

    if (data) {

      this.set(data);

    }

  }

  Plan.prototype = {

    set: function(data) {

      angular.extend(this, data);
      this.startDate = new Date(data.startDate);
      this.endDate = new Date(data.endDate);
      this.startTime = new Date(data.startTime);
      this.endTime = new Date(data.endTime);

    }

  };

  return Plan;

}

export default PlanService;
