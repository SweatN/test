import planListHtml from './plan-list.component.html';

let planListComponent = {
  template: planListHtml,
  controller: ($scope, $log, $state, $stateParams, Trips, $q) => {
    $scope.isHome = $state.current.name === 'home-page';
    $scope.planTypes = Trips.getPlanTypes();
    $q((resolve, reject) => {

      resolve($scope.$ctrl)

    }).then((data) => {

      $scope.trip = data.trip;
      $scope.tripId = Trips.getIndex($scope.trip);

    });

    $scope.removePlan = (planId) => {

      Trips.removePlan($scope.tripId, planId);

    };

  },
  bindings: {
    trip: '='
  }

}

export default planListComponent;
