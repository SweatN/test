import tripListHtml from './trip-list.component.html';

let tripListComponent = {
  template: tripListHtml,
  controller: ($scope, $log, Trips) => {

    $scope.title = 'Trip List';
    Trips.getAll().then((data) => {
      $scope.trips = data;
    });

    $scope.remove = (tripId) => {

      Trips.remove(tripId);

    };
  }

}

export default tripListComponent;
