function TripsService(Trip, Plan, $q, $log, localStorageService) {

  return {
    _trips: [],
    _planTypes: {
      flight: 'Flight',
      train: 'Train',
      hotel: 'Hotel',
      dinner: 'Restaurant/Cafe',
      any: 'Custom Activity'
    },

    _getInstance: function(tripId, tripData) {

      let instance = this._trips[tripId];

      if (instance) {

        instance.set(tripData);

      } else {

        instance = new Trip(tripData);
        this._trips[tripId] = instance;

      }

      return instance;
    },

    _search: function(tripId) {

      return this._trips[tripId];

    },

    _loadFromStorage: function() {

      let self = this;

      const loadJSONFromStorage = $q((resolve, reject) => {

        resolve(localStorageService.get('trips'));

      });

      return loadJSONFromStorage.then((data) => {
        let trips = angular.fromJson(data);
        angular.forEach(trips, (tripData, index) => {

          self._getInstance(index, tripData);

        });
        return self._trips;
      });

    },

    _updateStorage: function() {

      return localStorageService.set('trips', angular.toJson(this._trips));

    },

    create: function(tripData) {

      let trip = new Trip(tripData);
      this._trips.push(trip);
      this._updateStorage();

    },
    update: function() {

      let self = this;

      return $q((resolve, reject) => {

        resolve(self._updateStorage());

      }).then((response) => {

        return response;

      });

    },

    get: function(id) {

      let self = this,
        tripId = id - 1,
        trip = this._search(tripId);

      if (trip) {

        const loadTrip = $q((resolve, reject) => {

          resolve(trip);

        });

        return loadTrip.then((data) => {
          return data;
        });

      } else {

        return this._loadFromStorage().then((data) => {
          return self._search(tripId);
        });

      }

    },

    getIndex: function(trip) {

      return this._trips.indexOf(trip);

    },

    getAll: function() {

      if (this._trips.length) {

        const loadTrips = $q((resolve, reject) => {

          resolve(this._trips);

        });

        return loadTrips.then((data) => {
          return data;
        });

      } else {

        return this._loadFromStorage();

      }

    },

    remove: function(tripId) {

      this._trips.splice(tripId, 1);
      this._updateStorage();

    },

    getPlanTypes: function() {

      return this._planTypes;

    },

    createPlan: function(tripId, planData) {

      let plan = new Plan(planData);
      this._trips[tripId - 1]['plans'].push(plan);
      this._updateStorage();

    },

    removePlan: function(tripId, planId) {

      this._trips[tripId].plans.splice(planId, 1);
      this._updateStorage();

    }

  };

}

export default TripsService;
