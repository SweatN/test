import tripItemHtml from './trip-item.component.html';

let tripItemComponent = {
  template: tripItemHtml,
  controller: ($scope, $log, $stateParams, Trips, $state) => {

    $scope.title = $stateParams.title;
    $scope.isEditing = false;
    $scope.trip = {
      name: '',
      destination: '',
      startDate: new Date(),
      endDate: new Date(),
      description: '',
      plans: []
    };

    if (angular.isDefined($stateParams.id)) {
      $scope.isEditing = true;
      Trips.get($stateParams.id).then((trip) => {

        $scope.trip = trip;

      });
    }

    $scope.datepickerOptions = {
      minDate: new Date()
    };
    $scope.endDataDatepickerOptions = {
      minDate: new Date($scope.trip.startDate)
    };

    $scope.create = () => {

      if ($scope.trip.startDate > $scope.trip.endDate) {
        alert('Date is incorrect');
        return;
      }

      Trips.create($scope.trip);
      $state.go('home-page');

    };

    $scope.update = () => {

      if ($scope.trip.startDate > $scope.trip.endDate) {
        alert('Date is incorrect');
        return;
      }

      Trips.update();
      alert('Trip is updated');
      $state.go('home-page');

    };

    $scope.$watch(() => $scope.trip.startDate , (newValue, oldValue) => {

      $scope.endDataDatepickerOptions.minDate = new Date(newValue);

    });

  }

}

export default tripItemComponent;
