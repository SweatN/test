import angular from 'angular';

function TripService() {

  function Trip(data) {

    if (data) {

      this.set(data);

    }

  }

  Trip.prototype = {

    set: function(data) {

      angular.extend(this, data);
      this.startDate = new Date(data.startDate);
      this.endDate = new Date(data.endDate);

    }

  };

  return Trip;

}

export default TripService;
