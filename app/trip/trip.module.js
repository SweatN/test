import angular from 'angular';

import routing from './trip.route';
import tripListComponent from './list/trip-list.component';
import tripItemComponent from './item/trip-item.component';
import planListComponent from './plan/list/plan-list.component';
import planItemComponent from './plan/item/plan-item.component';
import PlanService from './plan/plan.service';
import TripService from './trip.service';
import TripsService from './trips.service';

angular
  .module('trip', [])
  .component('tripList', tripListComponent)
  .component('tripItem', tripItemComponent)
  .component('planList', planListComponent)
  .component('planItem', planItemComponent)
  .factory('Plan', PlanService)
  .factory('Trip', TripService)
  .factory('Trips', TripsService)
  .config(routing);
