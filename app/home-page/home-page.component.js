import homePageHtml from './home-page.component.html';

let homePageComponent = {
  template: homePageHtml,
  controller: ($scope, $log) => {

    $scope.title = 'Home Page';

  }

}

export default homePageComponent;
