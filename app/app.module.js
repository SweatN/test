import angular from 'angular';
import uirouter from 'angular-ui-router';
import uibootstrap from 'angular-ui-bootstrap';
import localStorageService from 'angular-local-storage';

import homePage from './home-page/home-page.component';
import trip from './trip/trip.module';

require('./main.scss');

angular
  .module('app', [
    uirouter,
    uibootstrap,
    localStorageService,
    'trip'
  ])
  .config(($stateProvider, $urlRouterProvider, $locationProvider) => {

    $locationProvider.hashPrefix('');
    //$locationProvider.html5Mode(true);

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home-page', {
        url: '/',
        component: 'homePage'
      })

  })
  .component('homePage', homePage)
